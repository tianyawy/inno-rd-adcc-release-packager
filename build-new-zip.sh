#!/bin/bash
set -e
rm -rf build
rm -rf innoRD-api
rm -rf innoRD-web

mkdir build



current=`pwd`
timestamp=`date '+%Y%m%d%H%M%S'`
git clone git@gitlab.com:daicode/innoRD-api.git
cd innoRD-api
docker run -it --rm -v $(pwd):/opt/app -w /opt/app tianyawy/suse423-node6 npm install 
cd $current
docker run --rm -v $(pwd):/to_zip -w /to_zip kramos/alpine-zip -r '/to_zip/build/inno-api-'`date '+%Y%m%d%H%M%S'`.zip /to_zip/innoRD-api

git clone git@gitlab.com:daicode/innoRD-web.git
cd innoRD-web
docker run -it --rm -v $(pwd):/opt/app -w /opt/app tianyawy/suse423-node6 yarn install 
docker run -it --rm -v $(pwd):/opt/app -w /opt/app suse423-node6 yarn build 
cd $current
docker run --rm -v $(pwd):/to_zip -w /to_zip kramos/alpine-zip -r '/to_zip/build/inno-web-'`date '+%Y%m%d%H%M%S'`.zip /to_zip/innoRD-web/dist
